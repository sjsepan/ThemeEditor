﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ThemeEditorLibrary")]
[assembly: AssemblyDescription("\nLibrary for tools to edit Visual Studio 2012 (and later) Themes, including Express Editions.\n\nBrian Chavez (http://twitter.com/bchavez) deserves credit for:\n~ all aspects of designing and coding in the original implementation (http://bchavez.bitarmory.com/archive/2012/08/27/modify-visual-studio-2012-dark-and-light-themes.aspx)\n\nSteve Sepan takes credit for:\n~ extending the original implementation to include selection of versions and editions\n~ adding handling for the Alpha channel\n~ all errors not present in the original version\n")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The MIT License (MIT)")]
[assembly: AssemblyProduct("ThemeEditorLibrary")]
[assembly: AssemblyCopyright("Copyright (c) 2012 Brian Chavez;\nportions Copyright (c) 2013 Stephen J Sepan")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("312b2233-3581-4151-a10e-09bbabce524d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.2")]
